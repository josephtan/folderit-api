<?php
include "auth.php";

$search_curl = curl_init();

curl_setopt_array($search_curl, array(
    CURLOPT_URL => $search_url,
    CURLOPT_RETURNTRANSFER => true,
    CURLOPT_ENCODING => "",
    CURLOPT_MAXREDIRS => 10,
    CURLOPT_TIMEOUT => 30,
    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
    CURLOPT_CUSTOMREQUEST => "GET",
    CURLOPT_HTTPHEADER => array(
        "authorization: Bearer ".returnResToken($token_url,$client_id,$client_secret),
        "content-type: application/json"
    ),
));

$search_files_response = curl_exec($search_curl);
$search_err = curl_error($search_curl);



//echo $meta_response;

$responseArray = json_decode($search_files_response,true);


function getMultiCurl($file_meta_url,$file_download_url,$fileid,$token_url,$client_id,$client_secret){

    $multiCurl = array();
// data to be returned
    $result = array();
// multi handle
    $mh = curl_multi_init();
    $ids = array();
    array_push($ids, $fileid);
    $httpHeaders = array(
        'content-type: application/json',
        "authorization: Bearer ".returnResToken($token_url,$client_id,$client_secret),
    );

    foreach ($ids as $i => $id) {
        // URL from which data will be fetched
        $fetchURL = $file_meta_url.$id;
        $multiCurl[$i] = curl_init();
        curl_setopt($multiCurl[$i], CURLOPT_URL,$fetchURL);
        curl_setopt($multiCurl[$i], CURLOPT_HEADER,0);
        curl_setopt($multiCurl[$i], CURLOPT_RETURNTRANSFER,1);
        curl_setopt($multiCurl[$i], CURLOPT_HTTPHEADER, $httpHeaders);
        curl_multi_add_handle($mh, $multiCurl[$i]);
    }


    $index=null;

    do {
        curl_multi_exec($mh,$index);
    } while($index > 0);

// get content and remove handles
    foreach($multiCurl as $k => $ch) {
        $result[$k] = curl_multi_getcontent($ch);
        curl_multi_remove_handle($mh, $ch);
        $responseArr = json_decode($result[$k]);
        echo $fetchURL.'<br/><br />';
        echo $result[$k]."<br/><br />";
        echo $file_download_url.$responseArr ->versionId."/preview";
    }
// close
    curl_multi_close($mh);
}



