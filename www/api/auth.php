<?php
// Obtain client Credentials access token with CURL
include "config.php";

function returnResToken($token_url,$client_id,$client_secret){
    $curl = curl_init();

    curl_setopt_array($curl, array(
        CURLOPT_URL => $token_url,
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => "",
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 30,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "POST",
        CURLOPT_POSTFIELDS => "grant_type=client_credentials",
        CURLOPT_HTTPHEADER => array('Authorization: Basic '.base64_encode($client_id.':'.$client_secret)),
    ));

    $response_token=curl_exec($curl);
//echo $result;
    if(!$response_token){die("Connection Failure");}
    $tokenArray = json_decode($response_token);
    $resToken = $tokenArray->access_token;
    curl_close($curl);
    return $resToken;
}




