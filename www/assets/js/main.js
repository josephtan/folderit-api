
let apiURL = 'https://api.folderit.com/';
let metaArr = [];
let matchedMetaID;

//add ajax setup so not to call URL token and method repeatedly
$.ajaxSetup({
    url: apiURL,
    type: 'GET',
    beforeSend: function (xhr) {
        xhr.setRequestHeader('Authorization', 'Bearer '+ authToken);
    }
});

$.when(
    $.get(apiURL+"/user/", function(response){
        userResponse =  response;
    })
).then(function() {
    getMetaAccount(userResponse["defaultAccountId"]);
});

function getSearchFilesPayload() {
    $.when(
        $.get(apiURL + "/search/files", function (response) {
            searchfilesRes = response;
        })
    ).then(function () {
        getFilesInfo(searchfilesRes);
    });
}

/*function sortFilesID(searchfilesRes){
    console.log(searchfilesRes);

}*/
function getFilesInfo(searchfilesRes){
    for(let i=0; i < searchfilesRes["hits"]; i++){
        let fileID = searchfilesRes["files"][i]["id"];

    $.when(
        $.get(apiURL + "/files/" + fileID, function (response) {
            allFilesResponse = response;
            if( matchedMetaID !== null && matchedMetaID !== undefined){
                getFilesInfoByMetaID(matchedMetaID,allFilesResponse);
            }
        }),

    )}
}

function getMetaAccount(accountID){
    $.when(
        $.get(apiURL+"/accounts/"+accountID+'/meta', function(response) {
            responseAccountInfo = response;
        })
    ).then(function(){
        for(let i = 0; i <responseAccountInfo.length; i++){
            $("#meta-available").append('<li class="list-group-item list-group-item-light">'+responseAccountInfo[i].name+'</li>');
            metaArr.push({
                id: responseAccountInfo[i].id,
                name :responseAccountInfo[i].name
            });
        }

        let metadata = new Bloodhound({
            datumTokenizer: Bloodhound.tokenizers.obj.whitespace('name'),
            queryTokenizer: Bloodhound.tokenizers.whitespace,
            identify: function(obj) { return obj.name; },
            local: metaArr,
        });
        function metadataSearch(query, sync) {
            metadata.search(query, sync);
        }

        $('.typeahead').typeahead({
            highlight: true, //Enable substring highlighting
            minLength: 1 //Specify minimum characters required for showing suggestions
        },
        {
            name: 'Meta',
            displayKey: 'name',
            source: metadataSearch,
            templates: {
            empty: [
                '<div class="empty">No Matches Found</div>'].join('\n'),
        },
        }).bind('typeahead:select', function(){
             let currentVal = $(this).val();
             matchMetaID(currentVal, metaArr);
             console.log("Fetching Data ...");
             getSearchFilesPayload();
             $("#file-details-view").append("<p>Fetching Data...</p>");
            if ($("#file-details-view", ".list-group-item".length ==1)){
                $("#file-details-view").empty();
            }
        });
    });
}

function matchMetaID(currentVal, metaArr){
    let selectedID;
        for( i=0; i < metaArr.length; i++){
            if (currentVal == metaArr[i]["name"]){
                selectedID = metaArr[i]["id"];
            }
        }
        matchedMetaID = selectedID;
}

function getFilesInfoByMetaID(matchedMetaID,allFilesResponse){
    let items = allFilesResponse;
        $.each(items.meta, function (i, objs) {
            let fileVersionId;
            let accountID;
            if (matchedMetaID == objs.meta.id) {

                    $("#file-details-view").append('<div class="list-group-item list-group-item-light">File id:'+items.id + ', ' + items.name + '</div>');
                fileVersionId = items.versionId;
                //accountID = items.accountId;
                console.log(items);
                console.log(fileVersionId);

            $.ajax({
                   url: apiURL+"files/versions/"+fileVersionId+"/preview",
                  //url:"https://folderit-files.s3.eu-west-1.amazonaws.com/"+accountID+"/preview."+fileVersionId+"/",
                  type: 'GET',
                  headers: {  'Access-Control-Allow-Origin': '*', 'Access-Control-Allow-Credentials':true },
                  beforeSend: function (xhr) {
                  xhr.setRequestHeader('Authorization', 'Bearer '+ authToken);
                  xhr.withCredentials = true;
                  },
                  success:function(response){
                      filePreviewResponse =  response;
                      console.log(filePreviewResponse);
                   //   $("#file-details-view").append('<a href="'+filePreviewResponse+'" target="_blank">Preview</a>');
                  }
                  });
            }
        });

    //console.log(matchedMetaID);
    //console.log(allFilesResponse);
    //return metaIDs, allFilesResponse;
};
