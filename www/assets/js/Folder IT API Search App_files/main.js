let localURL = 'https://folderit.jttan.com/assets/js/test-list.json';
let apiURL = 'https://api.folderit.com/';
let filesID = [];
let metaArr = [];
let metaIDs = [];

//add ajax setup so not to call URL token and method repeatedly
$.ajaxSetup({
    url: apiURL,
    type: 'GET',
    beforeSend: function (xhr) {
        xhr.setRequestHeader('Authorization', 'Bearer '+ authToken);
    }
});

$.when(
    $.get(apiURL+"/search/files", function(response){
        searfilesRes = response;
    })
).then(function () {
    sortFilesID(searfilesRes);
});


$.when(
    $.get(apiURL+"/user/", function(response){
        userResponse =  response;
    })
).then(function () {
    getMetaAccount(userResponse["defaultAccountId"]);
});

function sortFilesID(searfilesRes){
    for(let i=0; i < searfilesRes["hits"]; i++){
        let fileID = searfilesRes["files"][i]["id"];
        getFilesInfo(fileID);
    }
}
function getFilesInfo(fileID) {
    $.when(
        $.get(apiURL + "/files/" + fileID, function (response) {
            allFilesResponse = response;
        }),

    ).then(function () {
        let items = allFilesResponse;
        $.each(items.meta, function (i, obj) {
           metaIDs.push(obj.meta.id);
        });
    });

}

function getMetaAccount(accountID){
    $.when(
        $.get(apiURL+"/accounts/"+accountID+'/meta', function(response) {
            responseAccountInfo = response;
        })
    ).then(function(){
        for(let i = 0; i <responseAccountInfo.length; i++){
            $("#meta-available").append('<li class="list-group-item list-group-item-light">'+responseAccountInfo[i].name+'</li>');
            metaArr.push({
                id: responseAccountInfo[i].id,
                name :responseAccountInfo[i].name
            });

        }

        let metadata = new Bloodhound({
            datumTokenizer: Bloodhound.tokenizers.obj.whitespace('name'),
            queryTokenizer: Bloodhound.tokenizers.whitespace,
            identify: function(obj) { return obj.name; },
            local: metaArr,
        });
        function metadataSearch(query, sync) {
            metadata.search(query, sync);
        }

        $('.typeahead').typeahead({
            highlight: true, //Enable substring highlighting
            minLength: 1 //Specify minimum characters required for showing suggestions
        },
        {
            name: 'Meta',
            displayKey: 'name',
            source: metadataSearch,
            templates: {
            empty: [
                '<div class="empty">No Matches Found</div>'].join('\n'),
        },
        }).bind('typeahead:select', function(){
             let currentVal = $(this).val();
             matchMetaID(currentVal, metaArr);
        });
    });
}



function matchMetaID(currentVal, metaArr){
        console.log(currentVal);
        console.log(metaArr);
       // if(currentVal == metaArr[])
    
}