<?php
include "api/auth.php"; ?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Folder IT API Search App</title>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">

        <link rel="stylesheet" href="/assets/css/style.css">

    </head>
    <body>
        <section class="pt-4">
                <div class="container text-center">
                    <h1 class="title">Folderit API Search App</h1>
                </div>
        </section>
        <section class="section">
            <div class="container">
                <div class="columns">
                    <div class="column">
                        <h3 class="text-center">Request Data responses</h3>
                        <div class="container">
                                Available Meta:
                                <ul id="meta-available">

                                </ul>
                                <form action="" method="post">
                                    <div class="row">
                                        <div class="col">
                                            <label for="search-files">Search Available Files Through Meta</label>
                                        </div>
                                    </div>
                                    <div class="row search-row">
                                        <div class="col">
                                            <input class="form-control typeahead" type="text" autocomplete="off">
                                        </div>
                                    </div>
                                </form>
                                <div id="file-details-view">

                                </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
        <script type="text/javascript">
            let authToken = "<?php echo returnResToken($token_url,$client_id,$client_secret);?>";
        </script>
        <script src="./assets/js/typeahead.bundle.min.js" crossorigin="anonymous"></script>
        <script src="assets/js/main.js"></script>
    </body>
</html>